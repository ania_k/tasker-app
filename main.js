const modal = document.getElementById("myModal");
const url='https://api.myjson.com/bins/17sjww';

const imgToDo = "img/todo.svg";
const imgProgress = "img/progress.svg"
const imgDone = "img/done.svg"

let taskList = [];
let maxId = 0;

function fetchTasks () {
	let xhr = new XMLHttpRequest();
	if ('withCredentials' in xhr) {
		xhr.open("GET",url,true);
		xhr.onload = function () {
			let response = JSON.parse(xhr.responseText);
			if (xhr.readyState == 4 && xhr.status == "200") {
				taskList = response;
				for(i=0; i<taskList.length;i++) {
					displayTask(taskList[i]);
				}
				maxId = Math.max.apply(Math, taskList.map(function(task) { return task.id; }));
			} else {
				console.log("error");
			}
		}
		xhr.send();
	}
}

function openAddTask() {
	modal.style.display = "block";
}

function closeAddTask() {
  modal.style.display = "none";
}

function addTask() {
	let newTask = {
		"id":++maxId,
		"task": document.getElementById("newTask").value,
		"state":"todo"
	}
	taskList.push(newTask);
	let xhr = new XMLHttpRequest();
	xhr.open("PUT",url,true);
	xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
	xhr.onload = function () {
		let response = JSON.parse(xhr.responseText);
		if (xhr.readyState == 4 && xhr.status == "200") {
			console.log(response);
			displayTask(newTask);
		} else {
		console.log("error");
		}
	}
	xhr.send(JSON.stringify(taskList));
	document.getElementById("newTask").value = "";
	modal.style.display = "none";
	
}

function removeTask(id, button) {
	let index = taskList.findIndex(task => task.id==id);
	taskList.splice(index,1);
	let li = button.parentElement;
	li.parentNode.removeChild(li);
	let xhr = new XMLHttpRequest();
	xhr.open("PUT",url,true);
	xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
	xhr.onload = function () {
		let response = JSON.parse(xhr.responseText);
		if (xhr.readyState == 4 && xhr.status == "200") {
			console.log(response);
		} else {
			console.log("error");
		}
	}
	xhr.send(JSON.stringify(taskList));
}

function editTask(taskId, newState, button) {
	let task = taskList.find(task => task.id==taskId)
	let elementToRemove = button.parentElement;
	elementToRemove.parentNode.removeChild(elementToRemove);
	task.state = newState;
	displayTask(task);
	let xhr = new XMLHttpRequest();
	xhr.open("PUT",url,true);
	xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
		xhr.onload = function () {
		let response = JSON.parse(xhr.responseText);
		if (xhr.readyState == 4 && xhr.status == "200") {
			console.log(response);
		} else {
			console.log("error");
		}
	}

	xhr.send(JSON.stringify(taskList));

}	


function displayTask(task) {
	let str;
	if(task.state=="todo") {	
		str =    `<li class="list-item" onmouseover="showButtons(this)" onmouseout="hideButtons(this)">
		<span class="removeItem" onclick="removeTask(${task.id}, this)" style="display: none;">&times;</span>
		<div class="taskName"><p>${task.task}</p></div>
		<img id="doneImg" class="changeState" src="img/done.svg" alt="" style="display: none;" onclick="editTask(${task.id}, 'done', this)">
		<img id="progressImg" class="changeState" src="img/progress.svg" alt="" style="display: none;" onclick="editTask(${task.id}, 'progress', this)">
		</li>`
	} else if(task.state=="progress") {
		str =    `<li class="list-item" onmouseover="showButtons(this)" onmouseout="hideButtons(this)">
		<span class="removeItem" onclick="removeTask(${task.id}, this)" style="display: none;">&times;</span>
		<div class="taskName"><p>${task.task}</p></div>
		<img id="doneImg" class="changeState" src="img/done.svg" alt="" style="display: none;" onclick="editTask(${task.id}, 'done', this)">
		<img id="todoImg" class="changeState" src="img/todo.svg" alt="" style="display: none;" onclick="editTask(${task.id}, 'todo', this)">
		</li>`
	} else {
		str =    `<li class="list-item" onmouseover="showButtons(this)" onmouseout="hideButtons(this)">
		<span class="removeItem" onclick="removeTask(${task.id}, this)" style="display: none;">&times;</span>
		<div class="taskName"><p>${task.task}</p></div>
		<img id="progressImg" class="changeState" src="img/progress.svg" alt="" style="display: none;" onclick="editTask(${task.id}, 'progress', this)">
		<img id="todoImg" class="changeState" src="img/todo.svg" alt="" style="display: none;" onclick="editTask(${task.id}, 'todo', this)">
		</li>`
	}
	const list = document.getElementById(task.state)
	list.insertAdjacentHTML('afterbegin',str);
}

function showButtons(listElement) {
	const button = listElement.getElementsByTagName("span")[0];
	const img1 = listElement.getElementsByTagName("img")[0];
	const img2 = listElement.getElementsByTagName("img")[1];
	button.style.display = "block";
	img1.style.display = "block";
	img2.style.display = "block";
	listElement.style.backgroundColor = "#f2f2f2";
}

function hideButtons(listElement) {
	const button = listElement.getElementsByTagName("span")[0];
	const img1 = listElement.getElementsByTagName("img")[0];
	const img2 = listElement.getElementsByTagName("img")[1];
	button.style.display = "none";
	img1.style.display = "none";
	img2.style.display = "none";
	listElement.style.backgroundColor = "white";
}